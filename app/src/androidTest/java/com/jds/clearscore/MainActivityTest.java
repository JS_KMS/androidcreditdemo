package com.jds.clearscore;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.jds.clearscore.loader.Loaders;
import com.jds.clearscore.view.DonutView;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.core.IsNot.not;

/**
 * Created by jonids on 18/12/2016.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityTest extends ActivityTestRule<MainActivity> {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    @After
    public void afterActivityLaunched() {
        super.afterActivityLaunched();
    }

    @Test
    public void testActivityStarts() {
        Assert.assertNotNull(mActivityTestRule.getActivity());
    }

    @Test
    public void testDataIsShown() {
        LoaderUtils.waitForLoader(mActivityTestRule.getActivity().getSupportLoaderManager().getLoader(Loaders.LOADER_DATA));
        checkResultIsDisplayed();
    }

    @Test
    public void testSimulateData() {
        LoaderUtils.waitForLoader(mActivityTestRule.getActivity().getSupportLoaderManager().getLoader(Loaders.LOADER_DATA));
        DonutView donutView = (DonutView) mActivityTestRule.getActivity().findViewById(R.id.donut_info);
        int oldValue = donutView.getValue();
        onView(withId(R.id.btn_fake_result)).perform(click());
        //Wait for new result
        LoaderUtils.waitForLoader(mActivityTestRule.getActivity().getSupportLoaderManager().getLoader(Loaders.LOADER_DATA));
        Assert.assertNotSame(oldValue, donutView.getValue());
    }

    @Test
    public void testRefreshData() {
        LoaderUtils.waitForLoader(mActivityTestRule.getActivity().getSupportLoaderManager().getLoader(Loaders.LOADER_DATA));
        //Initial result succeeded, click refresh data
        onView(withId(R.id.btn_refresh_data)).perform(click());
        //Wait for result
        LoaderUtils.waitForLoader(mActivityTestRule.getActivity().getSupportLoaderManager().getLoader(Loaders.LOADER_DATA));
        //Check results
        checkResultIsDisplayed();
    }

    private void checkResultIsDisplayed() {
        onView(withId(R.id.layout_value)).check(matches(isDisplayed()));
        onView(withId(R.id.layout_error)).check(matches(not(isDisplayed())));
    }

}
