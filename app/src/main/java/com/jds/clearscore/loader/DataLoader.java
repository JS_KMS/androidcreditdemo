package com.jds.clearscore.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.jds.clearscore.core.CoreApplication;
import com.jds.clearscore.model.AccountInfo;

import java.io.IOException;

import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by jonids on 17/12/2016.
 */

public class DataLoader extends AsyncTaskLoader<Response<AccountInfo>> {
    public DataLoader(Context context) {
        super(context);
    }

    @Override
    public Response<AccountInfo> loadInBackground() {
        Response<AccountInfo> result = null;
        try {
            result = CoreApplication.getInstance().getApiService().getWmcToken().execute();
        } catch (IOException e) {
            Timber.e(e);
        }
        return result;
    }
}
