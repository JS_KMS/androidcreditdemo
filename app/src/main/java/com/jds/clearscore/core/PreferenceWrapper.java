package com.jds.clearscore.core;

import android.content.Context;
import android.content.SharedPreferences;

import com.jds.clearscore.BuildConfig;

/**
 * Created by jonids on 18/12/2016.
 */

public enum PreferenceWrapper {

    INSTANCE;

    private static final String KEY_PREF_NAME = BuildConfig.APPLICATION_ID;
    private static final String KEY_PREF_REFRESH_INFO = "KEY_PREF_REFRESH_INFO";

    private SharedPreferences sPreferences;

    PreferenceWrapper() {
        if (sPreferences == null) {
            sPreferences = CoreApplication.getInstance().getSharedPreferences(KEY_PREF_NAME,
                    Context.MODE_PRIVATE);
        }
    }

    public boolean shouldShowRefreshInfo() {
        return sPreferences.getBoolean(KEY_PREF_REFRESH_INFO, true);
    }

    public void setShouldShowRefreshInfo(boolean value) {
        sPreferences.edit()
                .putBoolean(KEY_PREF_REFRESH_INFO, value)
                .apply();
    }
}
