package com.jds.clearscore.core;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jds.clearscore.BuildConfig;
import com.jds.clearscore.api.ApiService;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

/**
 * Created by jonids on 17/12/2016.
 */

public class CoreApplication extends Application {

    private static CoreApplication instance;
    private Gson gson;
    private ApiService apiServiceInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        if (BuildConfig.DEBUG) {
            //Only show logs for debug builds
            Timber.plant(new Timber.DebugTree());
        } else {
            //Log to crash reporting library
        }
    }

    public static CoreApplication getInstance() {
        return instance;
    }

    public ApiService getApiService() {
        if (apiServiceInstance == null) {
            Retrofit apiService = new Retrofit.Builder()
                    //If debug panel is enabled then use the stored server setting, otherwise use production api
                    .baseUrl(BuildConfig.apiBaseUrl)
                    .addConverterFactory(GsonConverterFactory.create(getGson()))
                    .client(new OkHttpClient.Builder().build())
                    .build();
            apiServiceInstance = apiService.create(ApiService.class);
        }
        return apiServiceInstance;
    }

    /**
     * Returns an instance of Gson to be used by RetroFit
     */
    private Gson getGson() {
        if (gson == null) {
            gson = new GsonBuilder()
                    .create();
        }
        return gson;
    }
}
