package com.jds.clearscore.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;

import com.jds.clearscore.R;
import com.jds.clearscore.util.UiUtils;

import timber.log.Timber;

/**
 * Created by jonids on 17/12/2016.
 */

public class DonutView extends View {

    private RectF outerRect, innerRect;
    private Paint paintOuter;
    private Paint paintInner;
    //Full circle angle
    private int maxDegree = 360;
    private int outerCircleWidth;
    private int innerCircleWidth;

    private int startColor;
    private int middleColor;
    private int endColor;
    private int outerCircleColor;
    private int gap;

    private int minValue, maxValue;
    private int value;
    private int oldValue;

    private int animationSpeed;
    private float animationInterval;
    private boolean animating;
    private float animationValue;
    //By using an interpolator we can change how the animation plays (i.e. make it non-linear)
    private Interpolator interpolator = new AccelerateDecelerateInterpolator();

    private EventsListener eventsListener;
    private Handler handlerAnimations = new Handler();

    public interface EventsListener {
        void onAnimationCompleted();
    }

    public DonutView(Context ctx) {
        super(ctx);
        init(null);
    }

    public DonutView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public DonutView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public DonutView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(AttributeSet attrs) {

        //Load xml attributes
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.DonutView);
            startColor = a.getColor(R.styleable.DonutView_startColor, Color.RED);
            middleColor = a.getColor(R.styleable.DonutView_middleColor, Color.YELLOW);
            endColor = a.getColor(R.styleable.DonutView_endColor, Color.GREEN);
            outerCircleColor = a.getColor(R.styleable.DonutView_outerCircleColor, Color.BLACK);
            innerCircleWidth = a.getDimensionPixelSize(R.styleable.DonutView_innerCircleWidth, UiUtils.dpToPx(getContext(), 5));
            outerCircleWidth = a.getDimensionPixelSize(R.styleable.DonutView_outerCircleWidth, UiUtils.dpToPx(getContext(), 3));
            minValue = a.getInt(R.styleable.DonutView_minValue, 0);
            maxValue = a.getInt(R.styleable.DonutView_maxValue, 100);
            value = a.getInt(R.styleable.DonutView_value, 0);
            gap = a.getDimensionPixelSize(R.styleable.DonutView_gap, UiUtils.dpToPx(getContext(), 3));
            animationSpeed = a.getInt(R.styleable.DonutView_animationSpeed, 1);
            animationInterval = a.getFloat(R.styleable.DonutView_animationInterval, 3.0f);
            a.recycle();
        } else {
            //No attributes, use default colors
            startColor = Color.RED;
            middleColor = Color.YELLOW;
            endColor = Color.GREEN;
            outerCircleColor = Color.BLACK;
            innerCircleWidth = UiUtils.dpToPx(getContext(), 5);
            outerCircleWidth = UiUtils.dpToPx(getContext(), 3);
            gap = UiUtils.dpToPx(getContext(), 3);
            minValue = 0;
            maxValue = 100;
            value = 0;
            animationSpeed = 1;
            animationInterval = 3f;
        }

        paintOuter = new Paint();
        paintOuter.setAntiAlias(true);
        paintOuter.setColor(outerCircleColor);
        paintOuter.setStyle(Paint.Style.STROKE);
        paintOuter.setStrokeWidth(outerCircleWidth);

        paintInner = new Paint();
        paintInner.setAntiAlias(true);
        paintInner.setStyle(Paint.Style.STROKE);
        //paintInner.setStrokeCap(Paint.Cap.ROUND);
        paintInner.setStrokeWidth(innerCircleWidth);

        /* Prevent the view from applying optimisations, because if optimisations are enabled,
        onDraw() won't be called. */
        setWillNotDraw(false);

    }

    //region Getters/Setters

    public void setEventsListener(EventsListener eventsListener) {
        this.eventsListener = eventsListener;
    }

    /**
     * @param value
     * @param animate If true the transition from old to new value will be animated
     */

    public void setValue(int value, boolean animate) {

        if (value == this.value)
            return;

        animationValue = this.value;
        this.oldValue = this.value;
        this.value = value;

        if (!animate) {
            animating = false;
            finishAnimations();
            invalidate();
        } else {
            if (getHandler() != null)
                getHandler().removeCallbacksAndMessages(animationRunnable);
            animating = false;
            animateNewValue();
            Timber.d("animationValue: %f, newValue: %d", animationValue, value);
        }
    }

    public void setValue(int value) {
        setValue(value, true);
    }

    public int getValue() {
        return value;
    }

    public int getMinValue() {
        return minValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
        invalidate();
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
        invalidate();
    }

    public int getAnimationSpeed() {
        return animationSpeed;
    }

    public void setAnimationSpeed(int animationSpeed) {
        this.animationSpeed = animationSpeed;
    }

    public float getAnimationInterval() {
        return animationInterval;
    }

    public void setAnimationInterval(float animationInterval) {
        this.animationInterval = animationInterval;
    }

    //endregion

    /* If using animations to set a new value, we use a runnable which is called by a dedicated
       handler. This way, the animations don't block the UI and some of the steps can be skipped
       (without affecting performance) if the system UI Choreographer is doing too much work.
     */
    private Runnable animationRunnable = new Runnable() {
        @Override
        public void run() {
            if (!animating)
                return;

            if (animationValue < value) {
                //Going from lower number to higher
                animationValue = animationValue + animationInterval > value ? value : animationValue + animationInterval;
                invalidate();
                if (animationValue != value)
                    postDelayed(this, animationSpeed);
                else
                    finishAnimations();
            } else if (animationValue > value) {
                //Going from higher number to lower
                animationValue = animationValue - animationInterval < value ? value : animationValue - animationInterval;
                invalidate();
                if (animationValue != value)
                    postDelayed(this, animationSpeed);
                else
                    finishAnimations();
            } else {
                finishAnimations();
            }
        }
    };

    /**
     * Notify any registered listeners that the new value animation has completed
     */
    private void finishAnimations() {
        animating = false;
        if (eventsListener != null)
            eventsListener.onAnimationCompleted();
    }

    private void animateNewValue() {
        //Cancel any running animation
        handlerAnimations.removeCallbacksAndMessages(null);
        animating = true;
        post(animationRunnable);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.save();
        //Rotate canvas so our circle gradient starts at 90 degrees (pi/2)
        canvas.rotate(-90, getHeight() / 2, getHeight() / 2);

        float finalValue;
        if (animating) {
            float range = value > oldValue ? value - oldValue : oldValue - value;
            float percentedRange = range - (Math.abs(value - animationValue));
            float val = 100f / range * percentedRange;
            float interpolatorValue = interpolator.getInterpolation(val / 100f) * percentedRange;
            finalValue = oldValue > value ? oldValue - interpolatorValue : oldValue + interpolatorValue;
        } else {
            finalValue = value;
        }

        //Scale number to min/max limits
        float scaledFinalValue = finalValue == 0 ? 0f : (100f / (maxValue - minValue) * finalValue) / 100f;

        //Outer circle
        canvas.drawArc(outerRect, 0, 360, true, paintOuter);

        //Inner circle
        //Apply 360 degrees range to the scaledNumber
        canvas.drawArc(innerRect, 0, scaledFinalValue * maxDegree, false, paintInner);

        canvas.restore();
        super.onDraw(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int size;
        int width = getMeasuredWidth();
        int height = getMeasuredHeight();

        //Force view to be square
        if (width > height) {
            size = height;
        } else {
            size = width;
        }
        setMeasuredDimension(size, size);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        int size = w > h ? h : w;
        int outerSize = outerCircleWidth / 2;
        int innerSize = (outerCircleWidth + (innerCircleWidth / 2)) + gap;

        //Calculate new bounds for the circles
        outerRect = new RectF(outerSize, outerSize, size - outerSize, size - outerSize);
        innerRect = new RectF(innerSize, innerSize, size - innerSize, size - innerSize);

        //Calculate a new gradient with the new bounds
        SweepGradient gradient2 = new SweepGradient(size / 2, size / 2,
                new int[]{startColor, middleColor, endColor}, new float[]{0f, 0.5f, 1f});
        paintInner.setShader(gradient2);


    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        //If view is detached remove all scheduled runnables
        handlerAnimations.removeCallbacksAndMessages(null);
    }
}
