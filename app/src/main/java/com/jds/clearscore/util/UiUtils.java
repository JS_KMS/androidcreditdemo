package com.jds.clearscore.util;

import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.FloatRange;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;

/**
 * Created by jonids on 17/12/2016.
 */

public class UiUtils {
    public static float mDensity;

    /**
     * Will convert the given dp to px according to the display density
     *
     * @param ctx
     * @param dp
     * @return
     */
    public static int dpToPx(Context ctx, float dp) {
        if (mDensity == 0) {
            mDensity = ctx.getResources().getDisplayMetrics().density;
        }
        return Math.round(dp * mDensity);
    }

    /**
     * Will animate a view height change
     *
     * @param view              The view the animation will be applied to
     * @param interpolator
     * @param startHeight
     * @param targetHeight
     * @param animationDuration
     * @param listener          Allows to set a listener to be notified about the animation changes
     */
    public static void animateView(final View view, Interpolator interpolator, final int startHeight, final int targetHeight, int animationDuration, AnimatorListenerAdapter listener) {
        final ViewGroup.LayoutParams lp = view.getLayoutParams();
        ValueAnimator animation = ValueAnimator.ofInt(startHeight, targetHeight);
        animation.setDuration(animationDuration);
        animation.setInterpolator(interpolator);
        if (listener != null)
            animation.addListener(listener);
        animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @SuppressWarnings("Range")
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                lp.height = (int) animation.getAnimatedValue();
                view.setLayoutParams(lp);
                view.setAlpha(startHeight < targetHeight ? animation.getAnimatedFraction() : (1f - animation.getAnimatedFraction()));
            }
        });
        animation.start();
    }

    /**
     * Blend {@code color1} and {@code color2} using the given ratio.
     *
     * @param ratio of which to blend. 1.0 will return {@code color1}, 0.5 will give an even blend,
     *              0.0 will return {@code color2}.
     */
    public static int blendColors(int color1, int color2, @FloatRange(from = 0, to = 1) float ratio) {
        final float invertedRatio = 1f - ratio;
        float r = (Color.red(color1) * ratio) + (Color.red(color2) * invertedRatio);
        float g = (Color.green(color1) * ratio) + (Color.green(color2) * invertedRatio);
        float b = (Color.blue(color1) * ratio) + (Color.blue(color2) * invertedRatio);
        return Color.rgb((int) r, (int) g, (int) b);
    }
}
