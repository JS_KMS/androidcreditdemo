
package com.jds.clearscore.model;

import android.os.Parcel;
import android.os.Parcelable;

public class AccountInfo implements Parcelable {


    public String accountIDVStatus;

    public CreditReportInfo creditReportInfo;

    public String dashboardStatus;

    public String personaType;

    public CoachingSummary coachingSummary;


    @Override
    public String toString() {
        return "AccountInfo{" +
                "accountIDVStatus='" + accountIDVStatus + '\'' +
                ", \ncreditReportInfo=" + creditReportInfo +
                ", dashboardStatus='" + dashboardStatus + '\'' +
                ", personaType='" + personaType + '\'' +
                ", \ncoachingSummary=" + coachingSummary +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.accountIDVStatus);
        dest.writeParcelable(this.creditReportInfo, flags);
        dest.writeString(this.dashboardStatus);
        dest.writeString(this.personaType);
        dest.writeParcelable(this.coachingSummary, flags);
    }

    public AccountInfo() {
    }

    protected AccountInfo(Parcel in) {
        this.accountIDVStatus = in.readString();
        this.creditReportInfo = in.readParcelable(CreditReportInfo.class.getClassLoader());
        this.dashboardStatus = in.readString();
        this.personaType = in.readString();
        this.coachingSummary = in.readParcelable(CoachingSummary.class.getClassLoader());
    }

    public static final Parcelable.Creator<AccountInfo> CREATOR = new Parcelable.Creator<AccountInfo>() {
        @Override
        public AccountInfo createFromParcel(Parcel source) {
            return new AccountInfo(source);
        }

        @Override
        public AccountInfo[] newArray(int size) {
            return new AccountInfo[size];
        }
    };
}
