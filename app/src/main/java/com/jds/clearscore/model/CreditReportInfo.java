
package com.jds.clearscore.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CreditReportInfo implements Parcelable {


    public int score;

    public int scoreBand;

    public String clientRef;

    public String status;

    public int maxScoreValue;

    public int minScoreValue;

    public int monthsSinceLastDefaulted;

    public boolean hasEverDefaulted;

    public int monthsSinceLastDelinquent;

    public boolean hasEverBeenDelinquent;

    public int percentageCreditUsed;

    public int percentageCreditUsedDirectionFlag;

    public int changedScore;

    public int currentShortTermDebt;

    public int currentShortTermNonPromotionalDebt;

    public int currentShortTermCreditLimit;

    public int currentShortTermCreditUtilisation;

    public int changeInShortTermDebt;

    public int currentLongTermDebt;

    public int currentLongTermNonPromotionalDebt;

    public int changeInLongTermDebt;

    public int numPositiveScoreFactors;

    public int numNegativeScoreFactors;

    public int equifaxScoreBand;

    public String equifaxScoreBandDescription;

    public int daysUntilNextReport;

    @Override
    public String toString() {
        return "CreditReportInfo{" +
                "score=" + score +
                ", scoreBand=" + scoreBand +
                ", clientRef='" + clientRef + '\'' +
                ", status='" + status + '\'' +
                ", maxScoreValue=" + maxScoreValue +
                ", minScoreValue=" + minScoreValue +
                ", monthsSinceLastDefaulted=" + monthsSinceLastDefaulted +
                ", hasEverDefaulted=" + hasEverDefaulted +
                ", monthsSinceLastDelinquent=" + monthsSinceLastDelinquent +
                ", hasEverBeenDelinquent=" + hasEverBeenDelinquent +
                ", percentageCreditUsed=" + percentageCreditUsed +
                ", percentageCreditUsedDirectionFlag=" + percentageCreditUsedDirectionFlag +
                ", changedScore=" + changedScore +
                ", currentShortTermDebt=" + currentShortTermDebt +
                ", currentShortTermNonPromotionalDebt=" + currentShortTermNonPromotionalDebt +
                ", currentShortTermCreditLimit=" + currentShortTermCreditLimit +
                ", currentShortTermCreditUtilisation=" + currentShortTermCreditUtilisation +
                ", changeInShortTermDebt=" + changeInShortTermDebt +
                ", currentLongTermDebt=" + currentLongTermDebt +
                ", currentLongTermNonPromotionalDebt=" + currentLongTermNonPromotionalDebt +
                ", changeInLongTermDebt=" + changeInLongTermDebt +
                ", numPositiveScoreFactors=" + numPositiveScoreFactors +
                ", numNegativeScoreFactors=" + numNegativeScoreFactors +
                ", equifaxScoreBand=" + equifaxScoreBand +
                ", equifaxScoreBandDescription='" + equifaxScoreBandDescription + '\'' +
                ", daysUntilNextReport=" + daysUntilNextReport +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.score);
        dest.writeInt(this.scoreBand);
        dest.writeString(this.clientRef);
        dest.writeString(this.status);
        dest.writeInt(this.maxScoreValue);
        dest.writeInt(this.minScoreValue);
        dest.writeInt(this.monthsSinceLastDefaulted);
        dest.writeByte(this.hasEverDefaulted ? (byte) 1 : (byte) 0);
        dest.writeInt(this.monthsSinceLastDelinquent);
        dest.writeByte(this.hasEverBeenDelinquent ? (byte) 1 : (byte) 0);
        dest.writeInt(this.percentageCreditUsed);
        dest.writeInt(this.percentageCreditUsedDirectionFlag);
        dest.writeInt(this.changedScore);
        dest.writeInt(this.currentShortTermDebt);
        dest.writeInt(this.currentShortTermNonPromotionalDebt);
        dest.writeInt(this.currentShortTermCreditLimit);
        dest.writeInt(this.currentShortTermCreditUtilisation);
        dest.writeInt(this.changeInShortTermDebt);
        dest.writeInt(this.currentLongTermDebt);
        dest.writeInt(this.currentLongTermNonPromotionalDebt);
        dest.writeInt(this.changeInLongTermDebt);
        dest.writeInt(this.numPositiveScoreFactors);
        dest.writeInt(this.numNegativeScoreFactors);
        dest.writeInt(this.equifaxScoreBand);
        dest.writeString(this.equifaxScoreBandDescription);
        dest.writeInt(this.daysUntilNextReport);
    }

    public CreditReportInfo() {
    }

    protected CreditReportInfo(Parcel in) {
        this.score = in.readInt();
        this.scoreBand = in.readInt();
        this.clientRef = in.readString();
        this.status = in.readString();
        this.maxScoreValue = in.readInt();
        this.minScoreValue = in.readInt();
        this.monthsSinceLastDefaulted = in.readInt();
        this.hasEverDefaulted = in.readByte() != 0;
        this.monthsSinceLastDelinquent = in.readInt();
        this.hasEverBeenDelinquent = in.readByte() != 0;
        this.percentageCreditUsed = in.readInt();
        this.percentageCreditUsedDirectionFlag = in.readInt();
        this.changedScore = in.readInt();
        this.currentShortTermDebt = in.readInt();
        this.currentShortTermNonPromotionalDebt = in.readInt();
        this.currentShortTermCreditLimit = in.readInt();
        this.currentShortTermCreditUtilisation = in.readInt();
        this.changeInShortTermDebt = in.readInt();
        this.currentLongTermDebt = in.readInt();
        this.currentLongTermNonPromotionalDebt = in.readInt();
        this.changeInLongTermDebt = in.readInt();
        this.numPositiveScoreFactors = in.readInt();
        this.numNegativeScoreFactors = in.readInt();
        this.equifaxScoreBand = in.readInt();
        this.equifaxScoreBandDescription = in.readString();
        this.daysUntilNextReport = in.readInt();
    }

    public static final Parcelable.Creator<CreditReportInfo> CREATOR = new Parcelable.Creator<CreditReportInfo>() {
        @Override
        public CreditReportInfo createFromParcel(Parcel source) {
            return new CreditReportInfo(source);
        }

        @Override
        public CreditReportInfo[] newArray(int size) {
            return new CreditReportInfo[size];
        }
    };
}
