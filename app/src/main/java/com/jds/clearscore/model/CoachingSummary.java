
package com.jds.clearscore.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CoachingSummary implements Parcelable {


    public boolean activeTodo;

    public boolean activeChat;

    public int numberOfTodoItems;

    public int numberOfCompletedTodoItems;

    public boolean selected;

    @Override
    public String toString() {
        return "CoachingSummary{" +
                "activeTodo=" + activeTodo +
                ", activeChat=" + activeChat +
                ", numberOfTodoItems=" + numberOfTodoItems +
                ", numberOfCompletedTodoItems=" + numberOfCompletedTodoItems +
                ", selected=" + selected +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.activeTodo ? (byte) 1 : (byte) 0);
        dest.writeByte(this.activeChat ? (byte) 1 : (byte) 0);
        dest.writeInt(this.numberOfTodoItems);
        dest.writeInt(this.numberOfCompletedTodoItems);
        dest.writeByte(this.selected ? (byte) 1 : (byte) 0);
    }

    public CoachingSummary() {
    }

    protected CoachingSummary(Parcel in) {
        this.activeTodo = in.readByte() != 0;
        this.activeChat = in.readByte() != 0;
        this.numberOfTodoItems = in.readInt();
        this.numberOfCompletedTodoItems = in.readInt();
        this.selected = in.readByte() != 0;
    }

    public static final Parcelable.Creator<CoachingSummary> CREATOR = new Parcelable.Creator<CoachingSummary>() {
        @Override
        public CoachingSummary createFromParcel(Parcel source) {
            return new CoachingSummary(source);
        }

        @Override
        public CoachingSummary[] newArray(int size) {
            return new CoachingSummary[size];
        }
    };
}
