package com.jds.clearscore.api;

import com.jds.clearscore.model.AccountInfo;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by jonids on 18/12/2016.
 */

public interface ApiService {

    @GET("mockcredit/values")
    Call<AccountInfo> getWmcToken();
}
