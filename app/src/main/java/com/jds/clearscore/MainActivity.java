package com.jds.clearscore;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.jds.clearscore.core.PreferenceWrapper;
import com.jds.clearscore.loader.DataLoader;
import com.jds.clearscore.loader.Loaders;
import com.jds.clearscore.model.AccountInfo;
import com.jds.clearscore.util.UiUtils;
import com.jds.clearscore.view.DonutView;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Response<AccountInfo>> {

    private static final String KEY_ACCOUNT_INFO = "KEY_ACCOUNT_INFO";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.layout_swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.layout_pull_info)
    View layoutPullInfo;
    @BindView(R.id.donut_info)
    DonutView donutInfo;
    @BindView(R.id.layout_loading)
    LinearLayout layoutLoading;
    @BindView(R.id.layout_value)
    LinearLayout layoutValue;
    @BindView(R.id.layout_error)
    LinearLayout layoutError;
    @BindView(R.id.txt_value)
    TextSwitcher txtValue;
    @BindView(R.id.txt_max_value)
    TextView txtMaxValue;
    @BindView(R.id.btn_info_close)
    ImageButton btnInfoClose;
    @BindView(R.id.btn_fake_result)
    Button btnFakeResult;
    @BindView(R.id.btn_refresh_data)
    Button btnRefreshData;
    @BindView(R.id.btn_try_again)
    Button btnTryAgain;

    private boolean firstRun = true;
    private AccountInfo accountInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_home);

        /*If there's a savedInstance try to  retrieve saved values, otherwise load data from
         the calling Intent */
        restoreInstanceState(savedInstanceState != null ? savedInstanceState : getIntent().getExtras());

        txtValue.setFactory(new ViewSwitcher.ViewFactory() {

            public View makeView() {
                TextView myText = new TextView(MainActivity.this);
                myText.setGravity(Gravity.CENTER_HORIZONTAL);
                myText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 50);
                myText.setTextColor(getResources().getColor(R.color.colorPrimary));
                return myText;
            }
        });
        txtValue.setText("--");
        Animation in = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        Animation out = AnimationUtils.loadAnimation(this, R.anim.slide_down);

        txtValue.setInAnimation(in);
        txtValue.setOutAnimation(out);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData(false, true);
            }
        });

        donutInfo.setEventsListener(new DonutView.EventsListener() {
            @Override
            public void onAnimationCompleted() {
                applyTextValueColor();
                txtValue.setText(String.valueOf(donutInfo.getValue()));
                txtMaxValue.setText(getString(R.string.txt_your_credit_score_out_of, donutInfo.getMaxValue()));

                checkSwipeDownInfo();
            }
        });

        btnInfoClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hidePullRefreshInfo();
            }
        });

        btnFakeResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                donutInfo.setValue(new Random().nextInt(donutInfo.getMaxValue()));
            }
        });

        btnRefreshData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData(true, true);
            }
        });

        btnTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData(true, true);
            }
        });

        /*If accountInfo is null then try to load data on start, if it's not null.
        it means the activity we can reuse the data that was saved before the activity was stopped*/
        if (accountInfo == null)
            loadData(true, false);
        else
            showData(false);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_ACCOUNT_INFO, accountInfo);
    }


    private void restoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(KEY_ACCOUNT_INFO))
                accountInfo = savedInstanceState.getParcelable(KEY_ACCOUNT_INFO);
        }
    }

    /**
     * @param showLoadingPanel If true, will show the loading panel, otherwise will just load the
     *                         data without displaying a loading progress
     * @param forceLoad        If false, it will try to reuse any ongoing request (e.g. orientation change),
     *                         otherwise will destroy any ongoing requests and start a new one
     */
    private void loadData(boolean showLoadingPanel, boolean forceLoad) {
        if (showLoadingPanel) {
            layoutLoading.setVisibility(View.VISIBLE);
            layoutValue.setVisibility(View.GONE);
        }
        layoutError.setVisibility(View.GONE);

        if (forceLoad)
            getSupportLoaderManager().restartLoader(Loaders.LOADER_DATA, null, this).forceLoad();
        else
            getSupportLoaderManager().initLoader(Loaders.LOADER_DATA, null, this).forceLoad();
    }

    private void showData(boolean animate) {
        if (accountInfo != null && accountInfo.creditReportInfo != null) {
            donutInfo.setMinValue(accountInfo.creditReportInfo.minScoreValue);
            donutInfo.setMaxValue(accountInfo.creditReportInfo.maxScoreValue);
            donutInfo.setValue(accountInfo.creditReportInfo.score, animate);

            layoutLoading.setVisibility(View.GONE);
            layoutValue.setVisibility(View.VISIBLE);
            layoutError.setVisibility(View.GONE);
        } else
            showError();
    }

    private void showData() {
        showData(true);
    }

    private void showError() {
        layoutLoading.setVisibility(View.GONE);
        layoutValue.setVisibility(View.GONE);
        layoutError.setVisibility(View.VISIBLE);
        donutInfo.setValue(donutInfo.getMinValue());
    }

    private void showRefreshInfoPopup() {
        if (layoutPullInfo.getVisibility() == View.GONE) {
            Interpolator animationInterpolator = new FastOutSlowInInterpolator();
            int animationDuration = getResources().getInteger(android.R.integer.config_longAnimTime);
            layoutPullInfo.setVisibility(View.VISIBLE);
            UiUtils.animateView(layoutPullInfo, animationInterpolator, 0, getResources().getDimensionPixelSize(R.dimen.info_panel_height), animationDuration, null);
            firstRun = false;
        }
    }

    private void checkSwipeDownInfo() {
        if (firstRun && PreferenceWrapper.INSTANCE.shouldShowRefreshInfo()) {
            showRefreshInfoPopup();
        } else if (layoutPullInfo.getVisibility() == View.VISIBLE)
            hidePullRefreshInfo();
    }

    /**
     * Calculates and applies the correct color to the value TextView
     */
    private void applyTextValueColor() {
        float percentage = 1f / donutInfo.getMaxValue() * donutInfo.getValue();
        int startColor = getResources().getColor(R.color.gradient_start);
        int middleColor = getResources().getColor(R.color.gradient_middle);
        int endColor = getResources().getColor(R.color.gradient_end);

        int finalColor;

        if (percentage <= 0.5f) {
            //Transform interval of 0-0.5 into 0-1.0f
            finalColor = UiUtils.blendColors(startColor, middleColor, 1 - (1f / 0.5f * percentage));
        } else {
            //Transform interval of 0.5f-1.0f into 0-1.0f
            finalColor = UiUtils.blendColors(middleColor, endColor, 1f - (percentage / 2f));
        }

        //Apply gradient color to the Vluae TextView
        ((TextView) txtValue.getNextView()).setTextColor(finalColor);
    }


    @Override
    public Loader<Response<AccountInfo>> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case Loaders.LOADER_DATA:
                return new DataLoader(this);
        }
        return null;
    }

    @Override
    public void onLoadFinished
            (Loader<Response<AccountInfo>> loader, Response<AccountInfo> data) {
        switch (loader.getId()) {
            case Loaders.LOADER_DATA:
                swipeRefreshLayout.setRefreshing(false);
                if (data != null && data.isSuccessful() && data.body() != null && data.body().creditReportInfo != null) {
                    accountInfo = data.body();
                    Timber.d("result %s", data.body());

                    /*If score remains the same the donut view callback won't be called, so we need
                    to update the refresh info layout here */
                    if (data.body().creditReportInfo.score == donutInfo.getValue())
                        checkSwipeDownInfo();

                    showData();
                } else {
                    showError();
                    return;
                }
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Response<AccountInfo>> loader) {

    }

    private void hidePullRefreshInfo() {
        Interpolator animationInterpolator = new FastOutSlowInInterpolator();
        int animationDuration = getResources().getInteger(android.R.integer.config_longAnimTime);
        UiUtils.animateView(layoutPullInfo, animationInterpolator, layoutPullInfo.getHeight(), 0, animationDuration, new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                layoutPullInfo.setVisibility(View.GONE);
            }
        });

        /*Store that the user has dismissed the refresh info panel, so that it doesn't show every
          time the app is restarted */
        PreferenceWrapper.INSTANCE.setShouldShowRefreshInfo(false);
    }
}
